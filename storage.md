### Storage
    - Primary partitions and Extended partitions 
    - 3 Primary partitions and 1 extended partitions
    - extended partitions will be used to take the complete 
    - DAS (Direct attach Storage) Ex: External HDD, Internal HDD, DVD etc
    - Centralized storage over the network
        - NAS -> network attached storage
            - file level storage Ex: NFS, SMB
            - NFS for file and printer sharing from linux to linux
            - File system
        - SAN 
            - Storage area network
            - Block level access
            - here hardisk can be shared
            - shared between linux and windows only
            - Fast, but expensive
            - SAN Switch
                - connect multiple servers and devices to one storage

### RAID
    - Redundant array of inexpensive disks (Redundant array of independent disk)
    - RAID is used to recover the data from the fault
    - here fault tolerance takes place in different 
    - Raid 0 cannot recover
    - create a partition with id fd, it will convert partition to raid auto detect
    - mdadm -C -a yes /dev/md0 -n3 /dev/sdc{p1,p2,p3} -l 5
    - mdadm -> command for raid
    - -a to mention the RAID device
    - -n no of devices or partitions
    - -l 5 is the level of RAID
    
    - then we need to format the md0 and then mount it with any mount point
    - here we use /raiddata
    - mkfs -t xfs /dev/mtd0
    - mount /dev/md0 /raiddata
    - cat /proc/mdstat for getting the stat
    - mdadm --detail /dev/md0
    - mdadm -f /dev/md0 /dev/sdc3 -> to make it faulty
    - mdadm -r /dev/md0 /dev/sdc3 -> to remove the faulty
    - mdadm -a /dev/md0 /dev/sdc3

### LVM
    -8e -> id
    - logical volume manager 
    - used to manage the large volume regular partitions cannot be extended but LVM partitions can
    - we can increase the partition space without loosing the data
    - extended partitions will be fully utilized
    - pvcreate is used to initialize the disk partition as a physical volume
    - The physical volumes are used to create a volume group and the LVM logical volumes are created on the volume groups.
    - fdisk /dev/sdc -> for creating a extended, then create them as LVM with id 8e
    - pvcreate /dev/sdc{5,6,7} -> create physcial volume
    - pvdisplay -> display the physical volume
    - pvs -> for physical volume summary
    - vgcreate vg0 /dev/sdc{5,6,7} -> created a virtual group vg0 from 5,6,7 LVM partition
    - vgdisplay vg0 -> display
    - vgs -> summary
    - lvcreate -L 40M -n /dev/vg0/lvm1 -> create a first logical volume lvm1 of size 40M , -L means extended  
    - lvcreate -L 40M -n /dev/vg0/lvm2
    - lvdisplay 
    - lvs 
    - vgs

    - then format the lvms xfs or ext4
    - mkfs -t xfs /dev/vg0/lvm1
    - mkfs -t ext4 /dev/vg0/lvm2
    - then we need to mount them

   **For growing xfs**  
    - xfs cannot be used to shrink the size
    - lvextend -L +20M /dev/vg0/lvm1 -> will extend the lv size
    - then grows the mount point -> xfs_grows /lvm1

   **For growing ext4** 
    - vgextend vg0 /dev/sdc8  
    - lvextend -L +20M /dev/vg0/lvm2
    - resize2fs /dev/vg0/lvm2
   
   **For reducing ext4** 
    - first unmount the device
    - e2fsck -f /dev/vg0/lvm2
    - resize2fs /dev/vg0/lvm2 40M
    - lvreduce -L -20M /dev/vg0/lvm2
    - mount it
