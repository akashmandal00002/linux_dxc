### permissions
    - chown <user_name> <file_name> -> change the owner for the file
    - chgrup <group_name> <file_name> -> change the group the user of that group will have the permission
    - -rw-r--r-- 1 prashant prashant 6 Jul  6 11:15 prashant
    - here first tells us the type of file - (for regular file), d (directory)
    - first 3 are of user permission, next three for group permissions and last three are the others permission
    - r -> read 4
    - w -> write 2
    - x -> execute 1
    - = for assign
    - + for appending the permissions
    - - for deleting the permissions
    - chmod u=rwx,g=rw,o=w <file_name>
    - chmod 762 <file_name> same as above by adding the octal representations 
    - chown <user_name>:<group_name> <file_name> -> will change the user as well as group name at once

### ACL (Access control list)
    - used to give permission to another particular user
    - getfacl <file_name> -> to get the file access control list
    - setfacl -m u:student:rwx <file_name>  -> -m is used to modify
    - setfacl -x u:student <file_name> -> -x is used to remove the student permissions
    - set default ACL inside a dir setfacl -d -m u:<user_name>:rw <dir_name>
        - after that whatever we created inside that dir will have the above as a default permissions

### special permission
    - **suid**
        - set user id, set for an user for a command and so that ordinary can execute the command with root previlege
        - s -> 4th bit of permission column indicated suid and execute permission are set
        - S only suid for only suid
        - if there is s then u can change the passwd
        - chmod 4755 /usr/bin/passwd -> whenever there are 4 permission then firrst one is special permission (suid)
    - **sgid**
        - s in the group permissions
        - used to give the permission of the group
        - the permission are inherited to the subdirectory
        - chmod 2775 /dir -> 2 is the special permission for sgid
    **stick bit** 
        - for dirs
        - t at tenth bit
        - set when are are multiple users have read write access
        - so that only the owner can delete and modify the files within it 
        - Ex: /tmp dir
        - chmod 1777 /tmp

### Sudo
    - super user do
    - with sudo privileges only a particular user can execute the particular command with root privileges
    -  /etc/sudoers = configuration file for sudo
    - visudo = Edit /etc/sudoers and prompt if there are som
    - here alias for user, commands and privileges
