### Disk partitioning
    - data is stored in the form of tracks
    - MBR -> master boot record contains the partition table it is stored 
    - fdisk -l -> list out all the disks
    - fdisk -l /dev/sda -> for hardisk
    - fdisk /dev/sda -> is used to partition
    - commands in partioning 
        - p is used to view the partition table
        - n is used to create a partition
        - d to delete a partition
        - w to save if every thing looks goods 
        - q to quit without saving 
        - partprobe is used to make changes in action without rebooting the maching (like source in .bashrc)
            - partprobe will update the file system with the kernel
    - mkfs -t xfs /dev/sda5 -> to format the sda5
        - mkfs -> make filesystem
        - -t -> filesystem type
        - xfx is the type

    
### mount 
    - to attach file system and removable device such as USB flash drives at a particular mount point in directory
        - mount <device name on which we mount> <mount point>
        - for Ex: mount /dev/sda3 /data
        - It will not be mounted after reboot
        - to save it we need to update it in /etc/fstab file
            - /dev/sda3     /data           defaults    0 0
            - filesystem    mount point     options     dumpfrequency->backup(0 means no backup, 1->daily, 2->alternate days)  fsck-> filesystem check(scan file,0->never check, 1-> check on priority, 2-9 check parallaly)
            - after that *mount -a* will be used to check whether there is no error if there is error then it will go to maintainace mode


