### Search and replace
    - %s/word_to_find/replacve_with/gi
        - gi -> more than one occurence per line 
    - 2,4s/replace_it/replace_with/gi
    - se sm -> match brackets
    - se ai -> auto indent
    - se nu -> set number
    - se ic -> set ignore case
