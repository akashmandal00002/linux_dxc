### Bootloader
- before there was LIB
    - GRUB ->  unified Bootloader
    - ls -l /etc/grub2.conf
        - GRUB2 Grand Unified Boot Loader
        - vmlinuz -> kernel
### Kernel
- loads systemmd

### cmd
- runlevel -> returns the previous level and then current level
- init <number> -> will go to that level
- systemctl get-default -> it will give the default  (graphical.target) whose runlevel=5
- systemctl set-default multi-user.target -> here the runlevel =3

### Root password troubleshooting (RHEL, centos)
    reboot
    press e at grub
    after line type rd.break console=tty1
    Ctrl + x
    mount -o remount,rw /sysroot
    chroot /sysroot
    passwd root
    touch /.autorelabel
    Press Ctrl + d twice
