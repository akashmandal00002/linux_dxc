## Virtualization

### Hypervisor (virtual machine monitor)
    - software or hardware that runs virtual machines.
    - allows one host computer to support multiple guest VMs by virtualling sharing the resources

### Installation methods
    - DVD or HDD
    - instal over network = NFS, FTP, HTTP
    - Kickstart installations
    - PXE installation

### Path 
    - **Absolute** -> starts from / and have to type the full path
    - **Relative** -> 
