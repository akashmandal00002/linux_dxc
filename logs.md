### logs
    - cd /var/log
    - lastb -> displays the last time you reboot the system
    - chrony = related to NTP server (network time server)
    - cups = related to printing, cups=common UNIX printing serviceo 
    - gdm -> GNOME display manager
    - sssd -> system service security Daemon
    - grep <username> /var/log/secure
    - system logging = monitoring the log files for security and troubleshooting
