/proc is the directory is used to infor about the processes in the kernel

cat /proc/devices -> details of major devices
cat /proc/mdstat -> RAID details
cat /proc/cpuinfo -> CPU and processor info
cat /proc/cpuinfo | grep lm -> lm = long mode indicates 64 bit processor
cat /proc/cpuinfo | grep vmx -> if supports virtualization it will generate the output
cat /proc/partitions -> about the partitions
cat /proc/version -> about the version of the OS
vi /etc/sysctl.conf -> used to change the system and network configs -> net.ipv4_forward=0 to disable and 1 for enable
- net.ipv4_forward -> used to forward the packets from the geteway
sysctl -a -> for all the network configs
sysctl -p -> used to save the changes in sysctl.conf

df -> for disk partitions 
du -> for file or directory
du -h <file or dir> -> for a specific file or inside the dir mentioned
du -sh <dir> -> for specific dir not inside the dir

### Basic system config output commands 
lscpu -> details of the CPU and processor
lsmem -> about the memory
free -g -> about the free mem in gb
vmstat -> about the virtual machine stats
hwclock
hwinfo
lshw

## process
    - ps -> for processs
    - ps -a -> for all
    - ps -ef | head -5
    - ps aux | head -5
    - ps -l
        - here NI -> nice value and it is the priority
            - -20 is the highest
            - 0 is the defualt
            - +19 is the lowest
    - nice -n -15 vi x
        - it will prioritize the vi 
        - and press Ctrl + z to suspend, dont save it if it is saved then the process will be completed
    - renice +20 <processId>
        - used top change the priority of a running process
    - kill <signal> <processid>
        - kill -l to get the signals
        
