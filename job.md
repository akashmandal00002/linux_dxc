### Job Scheduling
    - to schedule a job at a later time
    - at = to schedule a job once at a later time
    - at now +2min
        - Ex: echo "Hello" > /dev/pts/0 then <Enter> <Ctrl-d>
    - atd is the daemon for at service
    - Daemon is the service that runs continuously in the background for that particular server or service

    - atrm <jobid> = Delete the job


### Crontab
    - crontab -e 
        - then edit in the file
        - min(0-59)     hour(00-23)     DOM(1-31)   Month   DOW(0/7 for sunday, 1 for monday etc)  cmd
    - crontab -l 
        -> to list all the crontabs in queue
