### Administration
    - cat /etc/login.defs | more 
    - uid/gid -> for root = 0
    - uid /gid for regular user = 1000-60000
    - uid.gid for system users=201-999
    - cat /etc/default/useradd
    - grep /etc/passwd -> username:password(x):uid:gid:comment:homedir:shell 

### Groups
    - grep prashant /etc/group -> groupname:password:gid:additional members or secondary names
    - if username and primary groupname is same then it will be a private group
    - groups prashant -> tells username : groupname
    - useradd -g <primary_groupname> <user_name> -> to set the primary group at the time of creation of user
    - useradd options
        - -g -> primary group
        - -G -> secondary group
        - -d -> home dir
        - -s -> shell -> bash, zsh, nologin (for non-interactive shell)
        - -u -> uid
    - usermod -G sales prashant -> here the secondary group is added to the user prashant
    - shadow file
        - grep prashant /etc/shadow -> username:encrypted password:3-8 output of chage command(change aging): reserved for future use
    - chage <username> -> to change the age of the account and password
        - min age -> minimum days after user can change the passowrd
        - maximum password age -> in days
        - last password change -> just press enter
        - password inactive
        - account expires
    - groupadd -g 3000 it -> means the id is set to 3000 at the time of adding group it
    - groupmod -n dxc it -> -n is used to rename the group name
