### Network
    - ipv4 or ipv6 
    - ipv4 -> It is a 32 bit address
        - a.b.c.d (4 octets)
        - NWaddress is the address of whole network
        - hostaddress is the address of a specific system
    - ipv6 -> It is a 128 bit address

### IPV4 classes
    - class       public IP range           private IP range                Subnet mask         #of networks
    - class A   1.0.0.0 - 127.0.0.0         10.0.0.0 - 10.255.255.255       255.0.0.0           126
    - class B   128.0.0.0-191.255.0.0       172.16.0.0-172.31.255.255       255.255.0.0         16382
    - class C   192.0.0.0 - 223.255.255.0   192.168.0.0 - 192.168.255.255   255.255.255.0       2,097,150 

    - in class A we have max of 126 netowrks and about 16 million hosts
    - ping = packet Internet gopher
    - command to check conncectivity
    - ping <ipaddress>
    - ping 127.0.0.1

    - class B -> 
        - 2 octest for NW and 2 for Hosts
        - 16382 NW and 16282 hosts per NW
    - class C ->
        - 3 Octets for NW and 1 for host
        192.168.0.0 -> entire network
        192.168.255.255 -> Broadcast address
    - CIDR (classless inter domain routing)

- **ifconfig** -> tells the configurations
- **nmcli con show** -> 
       - nmcli -> network manager command line interface
       - con = connection
- **nmtui** -> NW manager text user interface
- **netstat** = gives network status
- **route -n or netstat -rn** = dispalys the routing table
- cat /etc/resolv.conf -> displays the IP Address of the NameServer(DNS Server)
- cat /etc/sysconfig -> tells about the system created by
- cat /etc/sysconfig/network-scripts/ifcfg-ens33
    - tells the config of the ens33
    - BOOTPROTO=none to configure static IP
    - BOOTPROTO=dhcp to configure dhcp
    - DHCP=dynamic host configure protocol = provides IP addresses dynamically from DHCP
