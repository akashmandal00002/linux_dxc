### Softlink
    - shortcut for the file
    - if original file is deleted then data is lost
    - ln -s <filename> <softlink>
    - inode nos of file and softlink is different
    - softlink can point to another partition

### Hard link
    - create another copy of the same file
    - if original file is deleted then data does not lost
    - ln <filename> hardlink
    - inode nos of file and hardlink are same 
    - hardlink cannot be created to another partition
