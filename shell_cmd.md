### BASH shell
    - up arrow key to recall previous command
    - Ctrl + a = takes cursor to beginning of command
    - Ctrl + e = takes cursor to end of the command
    - Ctrl + k = delete from cursor position to the end of command
    - Ctrl + u = deletes from cursor to the beginning of command
    - for a variable to be executed in shell and as well as in subshell then we need to export that 

### Scripts
- first we need to give the interpreter 
- #!/bin/bash -> magic sh bang sequence = tells OS which interpreter to use
- then run it with sh < name_of_the-file >
- or make it executale :- chmod +x samplescript

- readscript
- echo "Enter name: "
- read name -> read is used to read it from the command(inputs)
- echo "$name, Welcome" -> $name is used to pass that value of input name in here

### Operators
#### Arithmetic
- addition (+)
- subtraction (-)
- product ( * ) ->
- division (/)

#### Comparision operators
- **for strings**
- equal to (=)
- not equal to (!=)

- **Integers** 
- lt (lesser than)
- le (less than equal to)
- gt (greater than )
- ge (greater than or equal to)
- eq (equal to)
- ne (not equal to)

#### Logical operator
- && (logical and)
- || (logical or)

### Conditional
    ```
    if [condition]
        then
            statement
    elif [condition]
        then
            statement
    else
        statement
    ``` 

- we can use $1 and $2  for command line arguments
- $0 -> program name
- $1, $2 ---- -> argument no
- $* -> all arguments
- $# -> no of arguments

### Loops
```bash
for i in 1 2 3 4 5
do
echo $i
done
```

for i in {1..10}
do 
$i 
done

- In this there is a 2 no. gap
for i in {1..10..2}
do
echo $i
done

- like for loop in other lang
for ((i=1;i<=10;i++))
do
echo $i
done

for i in $(seq <start> <step> <end> )
do
echo $i
done

### Switch case
case $choice in 1) command;;
                2) command;;
                \*) invalid entry;;
esac


echo $? -> tells whether the previous command executed successfully or not
    - if output = 0 -> then it is successful
    - if 1-255 -> failure

### Variables
- **declare variables**
   - declare -r a=5 -> read only we cannot reassign it
   - declare -u b="hello" -> for uppercase
   - delcare -l c="MAN" -> for lowercase


### Escape character
- echo -e  "How are you doing? \t "have a great day " \n "GoodLuck""

### Eval
- we can use eval to evaluate a command

