### Toubleshooting (maintainace mode)
- mount -a -> check for the mount error
- vi /etc/fstab
- mount -o remount,rw /
- mkfs -t xfs /dev/(part) -f
- then mount it again
- df -Th

### Root password troubleshooting (RHEL, centos)
    reboot
    press e at grub
    after line type rd.break console=tty1
    Ctrl + x
    mount -o remount,rw /sysroot
    chroot /sysroot
    passwd root
    touch /.autorelabel
    Press Ctrl + d twice
