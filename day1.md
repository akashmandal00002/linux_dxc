## IT infrastructes
    - Servers
    - Clients
    - Networks
    - Storages
    - Backupos storage
    - Virtualization
    - Cooling Equipments
    - Hardwares

### NAS vs SAN
    - Network attached Storage (NAS)
        - used exclusively as a single centralized storage location for multiple devices on a network.
    - Storage Area Network(SAN)
        - a high-speed network of several interconnected devices operating as a data storage unit. SAN connects directly to a server

## Cybersecurity

### Firewalls
    - It is a network security device which monitors incoming and outgoing traffic and decide whether to allow or block specific traffic based on defined set of security rules

### Information Security
    - Confidentiality ->  keeping sensitive information cofidential and prtecteed from unauthorized user
    - Integrity -> Maintianing the accuracy and consistency of data
    - Availability -> Ensuring the authorized users have access to the information they need, when they need
    - Compliances -> 
    - **AAA** -> Authentication, authorization, Accountability(what did the user do on the system)

### Operational Security
    -  what a person is allowed to access
    - where data is stored and shared

### Disaster recovery and business continuity
    - can be man man(cyber-attack) as well as natural (natural calamities such as earthquakes)
    - backups and recovery
    - business continuity means the minimum needfulls are working as before the disaster

### End user Education
    - teaching users to delete suspicious mails and threats

## Types of Cyber threats
    - Cybercrimes -> individual or a group of person to gain financial gains or to cause disruption
    - cyber attacks -> involves politically motivated information gathering
    - Cyberterrorism -> intended toundermine electronic systems to cause panic or fear
    - Malware -> malicious software, gets the users private information

### Types of malware
    - Virus -> a program which replicate itself
    - trojans -> desguise themselves as legitimate softwares and cause damage
    - spyware -> secretly records the users movements and capture the credit card details
    - ransomware -> tries to capture the data and tries asks for money in exchange for sensitive data
    - Botnets -> installed bots on the users machines which will be required to do the cyber attacks at the large scale
    - Adwares -> advertisements
    - SQL injections

### External and Internal Issues
    - means ISO certifies 
    - External -> market and customer trends

###  Firmware
    - It is the software which provides the low level control for a device's specific hardware. such as BIOS

### PLD (programmable logic device)
    - SIMM -> single in-line memory module
    - DIMM -> double in-line memory module

### Device Programmables 
    - CMOs (complementary metal Oxide semiconductor) -> first thing which runs when bios is started it detect from which memory we first need to watch for.
    - PROM
    - EPROM
    ...

### Layered Architecture of Linux
    - Kernel -> core component, interacts between the hardware and the processes
    - Shell ->  interacts between the user and kernel (converts higher level language to user understandable format)
    - Utilities -> applications
    - user interacts with shell or applications -> kernel -> hardwares

### kernel
    - Core of OS
    - performs following functions:
        - memory management
        - processor management
        - I/O management
        - Job scheduling
        - Managing file systems
        - Handling interrupts
        - Managing file systems
    

### Generic Booting
    - Cold booting (Hard booting) -> when system is switched off and we switched on it.
    - Warm booting (soft boot)

### Boot Sequence
    - BIOS
    - POST -> Power on Self Test
    - MBR is loaded -> Master boot record, recorded
    - Boot loader -> loads operating system
    - Boot loader loads the OS
    - System configuration
    - Loading system Utilities
    - User authentication
