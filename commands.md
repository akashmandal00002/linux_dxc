run command in windows mstsc (window desktop connection)

touch {a..k} -> create files with name a,b,c.....k
ls -lR -> recursively inside a dir
ls -ld -> for dir
ls ? -> single character file
ls ?? -> two character file
ls [a-z] -> files files name a,b,c,d....k
ls [a-z]* -> starts with a-z
ls [!a-e]* -> file does not start with a,b,c,d,e

## Commands
    - cat /etc/redhat-release -> describe the version or the release 
    - free -> tells how much memory is free RAM in bytes
    - free -m -> tells in megabytes
    - free -g -> tells in gigabytes
    - swap -> It is a virtual memory, part of HDD used as RAM. Entire partition is allocated to swap. the inactive applications are moved to swap so that RAM is free to accept new Application.
    - uname -> tells us the which operating system we are using 
    - uname -r -> tells us the kernel information of the system
    - whoami -> tells us the current logged in user
    - id -> tells us the user id and the group id in which the currently logged in user belongs

    - usr -> user system resources foldeer = contains manpages, documentations etc

    - tty -> tells the type of terminal
    - chvt 1, 2, 3 -> changes to the terminal 1, 2, 3 respectively

### Day2
    - ps -> process status means that which shells we are using 
    - echo $SHELL -> tells which shell we are using 

### file manipulation
    - **cp**-> copy a file
        - -r for recursively (for copying a directory)
        - -i for interactive mode (asks for confirmation in overwriting)
    - **mv** -> to move a file or rename a file
        - if a file with destination name already exists then it will move file to directory
        - if a file with destination name not exists then it will rename it
    - **rm** -> to remove a file
        - for directory use -r (recursively)    
        - rmdir -> to remove an empty directory

### Users and group
    - useradd <user_name> -> used to add the user name
    - passwd <user_name> -> to change the password or set the password for the user
    - groupadd <group_name> -> to add a group


### Fundamental file types
    - (-) -> regular file
    - d -> directory
    - l -> symbolic links or soft links
    - b -> block special files
    - c -> character special files
    - p -> named piped used to pass data between processes
    - s -> socket for network communications 

### Device partition
    - ls -l /dev/sda*
        - brw-rw---- 1 root disk 8, 0 Jul  6 08:50 /dev/sda
        - brw-rw---- 1 root disk 8, 1 Jul  6 08:50 /dev/sda1
        - brw-rw---- 1 root disk 8, 2 Jul  6 08:50 /dev/sda2
        - b is here the block special file
        - 8 is the major number(refers to the device driver) and 0,1,2 is the minor number(is the device instance)

### device files
    - mknod /dev/fd0 b 2 0
        - here mknod is used to create a file
        - b is the block special file
        - 2 is the major number and 0 is the minor number
    - cat /proc/devices -> tells the major number for devices 

### Redirectors
    - Input -> Keyboard -> 0 -> <
    - Output -> Monitor -> 1 -> >
    - Error -> Monitor -> 2 -> 2>
    - All Output -> monitor -> &>

### Filters
    - filters will only affect the output not the file
    - cat (> : used to overwrite into the file) (>> : used to append into the file)
        - cat -n testfile -> number of lines also mentioned
        - -A is used to replace the empty lines with $
        - -s is used to squeze the no of empty lines
    - head -> used to display first n(default 10) lines
        - head -20 /etc/passwd -> prints the first 20 lines
    - tail -> used to display last n lines (similar to head)
    - cut -> used to cut column wise or field wise
        - cut -f1 -d: /etc/passwd 
        - cut -f1,4 -d: /etc/passwd -> cut the first and 4th field
        - cut -f1-4 -d: /etc/passwd -> cut from first till 4th
        - f1 is the field number
        - d -> d is the delimeter (or field seperator)
    - tr (transform) -
        - tr 'a-z' 'A-Z' to convert lowercase to uppercase letter ( and vice versa)
        - -d to delete
        - -s to squeeze "multiple consecutive occurences of a character to once"
        - echo hello | tr -s "l" -> helo
    - wc
        - tells the no of lines, words, bytes
        - -l -> for lines, -w -> words, -c -> only bytes
    - grep 
        - <text_search> <file_name> -> used to get the desired string matched
        - -i for ignore case (it will result all the matched pattern without the cases)
        - ^s -> prints all lines starting with s
        - j$ -> prints all lines ending with j
        - -c tells the count of the pattern string
        - n tells the occurences with line numbers
        - v is other than means it ignore that string and prints all others
    - uniq -> ill display one occurences of that line
        - -u will display only uniq lines
        - -d will display only duplicate lines
    - sort -> used to sort the lines in ascending order
        - -r used to sort it in reverse order (descending order)
        - -n for sort the numbers
    - awk -> used to manipule text
        - $1 is first field, $2 -> second field, $0 is the entire record
        - awk -F: '{print $3 " " $1}' /etc/passwd 
        - F is the field seperator
        - here we want 3rd field first followed by the first field
        - awk '/root/{print}' /etc/passwd
    - sed
        - used to find and replace
        - sed 's/mohit/rahane/gi' names
        - sed 3d names -> delete the 3rd line of file names
        - g= more than one occurence per lines
        - i is for ignore case
        - but it is not visible in file
        - sed -e to edit it into the fileccc
        - sed -n 3q names -> display the third line
        - sed 3q names -> print first 3 lines

## Inode
    - for user reference the filename is there, for OS reference inode no. or index no is there
    - ls -l <file_name>  tells us the filename
    - ls -i <file_name> tells us the inode no of file
    - stat <file_name> tells us the meta data and the stat of the file i.e inode

### tee
    - tee will store the input in between the pipes in the file list
    - ls -l / | tee list | sort
    - used to store data in between the pipes


### Umask
    - provides the default permissions to the file or directory
    - maximum permissions for dir= 777
    - umask for root user = 022 and for regular user =002
    - dir -> created by root(777-022=755), created by regular user(777-002=775) 
    - maximum permission for file
    - file -> created by root user = 666-002=664 

### Compression and decompression
    - gzip -v <mfile_name>
    - gunzip -v <file_name>
    - -v is verbose
    - bzip2 is used to compress a file bigger in size
    - bunzip2 is used to compress a file bigger in size
    - bzip2 and gzip is used to compress files not dirs
    - we cannot see the content of a zip file using **cat** 
    - but we need to use ***zcat*** to see the content of a gzip file
    - **less** is used to see content of bzip file

### tar (tape archive) or backup
    - used to backup a file or dir
    - it makes a copy does not disturb the original file
    - tar [Options] <destination> <source>
    - Example:  tar cvf /opt/an.tar anacondad-ks.cfg
    - here c=create backup, v=verbose, f=filename
    - tar tvf an.tar -> t=list backed up files, verbose, filename
    - tar xvf an.tar -> x = restore, v=verbose, f=filename
    - add a file to existing archive tar -rf <dest to the .tar file> <source_to_add>
    - tar --delete -f an.tar root/inital-setup-ks.cfg to delete the file
    - tar czvf /opt/newdir.tgz /newdir -> here z is for zip
    - tar cjvf <destination> <file name> -> for a bz2 file

### File attributes
    - used whether a user can modify or delete the file (+i) for immutable
    - lsattr <file_name>  used to list the file attributes
    - chattr (-,+)i <file_name> -> to add or delete the immutablility from the file
    - i -> immutable attribute (can't modify or delete), a -> append attribute(can't modify or delete, just append using *echo hello >> file1* or *cat >> file1*)

### Shred
    - If you delete a file, only the inode no is released, directory entry = file name to inode number is remove
    - shred will remove the file completely from HDD
    - deleted file will be destroyed completely from HDD and cannot be recovered
    - shred <file_name> 

### Find
    - used to find the file
    - find [path] <criteria>
    - find /root -user root -group prod -> find a file in /root file with user root and group prod
    - find /root -user root -o -group prod -> find a file in /root file with user root or group prod
    - -o is the logical or, means min either one of them
    - find /root -size (+,-<size>) 

### which 
    - used to display the absolute path of that command

### documentations
    - whatis <cmd>
    - man <cmd>
    - info <cmd>
    - cd /usr/share/doc/
    - cd zip
    - cat README | more
